Clazzy is an online learning platform that provides classes in Information Technology sector, mainly to help students with no IT knowledge or with minimal knowledge.
This chatBot is build to cater Clazzy FAQ, and this bot is trained using Clazzy's list of FAQ.

Clazzy's FAQ.
- How to join tonight class?
- Can I join tonight class?
- What type of classes available?
- How much the class cost?
- How to stop subscription?
- What payment method accepted?
- How to get help from tutor after class?